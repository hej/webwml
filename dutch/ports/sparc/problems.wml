#use wml::debian::template title="Overzetting naar SPARC -- Errata" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="bc5a5b465aa5f94e13b4ed873d83cbd36b1a2f2b"

<h1>Errata voor Debian SPARC</h1>
  <p>
Deze pagina bevat een lijst met bekende problemen met Debian SPARC. De nadruk
ligt op de stabiele release (of de bevroren toekomstige release, indien van
toepassing), omdat deze problemen veel minder vaak voorkomen en een groter
aantal mensen treffen.
  <p>
De volgende lijst met problemen is niet bedoeld om het Debian-systeem voor het
opsporen van fouten (Debian bug-tracking system - BTS) te vervangen. Het
belangrijkste doel van deze informatie is om verwarring bij gebruikers te
beperken, het verkeer op de mailinglijst te verminderen en tijdelijke
oplossingen uit te leggen totdat de bug is opgelost. Als aan een probleem een
bugnummer is gekoppeld, wordt dat bugnummer vermeld.


<h2>X Window-systeem</h2>
  <p>
Problemen met betrekking tot het X Window-systeem.

<h3>Hoe weet ik welke X-server ik moet gebruiken?</h3>
  <p>
Dit hangt af van uw machine en welke framebuffer (videokaart) u heeft
geïnstalleerd. De volgende lijst met X-serverpakketten zou moeten helpen -- kies
datgene dat overeenkomt met uw hardware en installeer het pakket (en stel het in
als de standaard X-server wanneer hierom wordt gevraagd tijdens de installatie).
<dl>
      <dt>xserver-xsun</dt>
      <dd>
Gebruikt voor de meeste Sun- en 8-bits-kloonframebuffers, zoals CG3, CG6, enz.
Bevat ook monochrome ondersteuning.
      <dt>xserver-xsun-mono</dt>
      <dd>
Gebruikt voor BWTWO en andere monochrome framebuffers, of voor het werken met
kleurenframebuffers in monochrome modus.
      <dt>xserver-xsun24</dt>
      <dd>
X-server die 24-bits kleur ondersteunt -- geschikt voor 24-bits-framebuffers die
worden ondersteund door de Linux-kernel. Niet veel mensen kunnen dit gebruiken.
      <dt>xserver-mach64</dt>
      <dd>
Gebruik deze X-server voor ATI Rage-framebuffers, zoals te vinden in veel op PCI
gebaseerde UltraSPARC-machines.
    </dl>

#  <p>
# There are experimental Creator accelerated X servers somewhere to be
# found. Is that in the distribution already?


<h3>Problemen met de Mach64-server</h3>
  <p>
Deze server zoekt alleen naar de framebuffer op <tt>/dev/fb0</tt>. Het kan zijn
dat u daar wat moet rommelen met symbolische koppelingen om het daarop te doen
lijken.
