#use wml::debian::template title="Het Debian Sociale Contract" BARETITLE=true
#use wml::debian::translation-check translation="c505e01dd6ca2b53d9a229a691d0c2b20c48b36b" maintainer="bas@debian.org"

{#meta#:
<meta name="keywords" content="social contract, Sociale Contract, dfsg, social contract,
Sociale Contract, dfsg, Debian Sociale Contract, dfsg, debian social contract, dfsg">
:#meta#}

#  Original document: contract.html
#  Author           : Manoj Srivastava ( srivasta@tiamat.datasync.com )
#  Created On       : Wed Jul  2 12:47:56 1997
#  Translator       : Bas Zoetekouw <bas@debian.org>
#  Translation date : Sat May 25 14:38:43 MET DST 2002
#  Last Translation Update by $Author$
#  Last Translation Update at $Date$

<p>
  Versie 1.1, geratificeerd op 26 april 2004. Vervangt
  <a href="social_contract.1.0">Versie 1.0</a>, geratificeerd op 5 juli 1997.
</p>


<p>Debian, de makers van het Debian systeem, hebben het
<strong>Debian Sociale Contract</strong> opgesteld. Het onderdeel
van dit contract dat handelt over de &quot;<a
href="#guidelines">Debian Richtlijnen voor Vrije Software</a>&quot;
(Debian Free Software Guidelines - DFSG), werd
oorspronkelijk ontwikkeld als een verzameling verplichtingen waarover
we hebben afgesproken ons eraan te houden.
Het werd door de vrije-software gemeenschap overgenomen als
de basis van de Open Source Definitie.
</p>

<hr />
<h2>Het <q>Sociale Contract</q> met de Vrije Software Gemeenschap</h2>
<ol>
   <li><strong>Debian zal 100% vrij blijven</strong>
     <p>De richtlijnen, die we gebruiken om te bepalen of een werk
     <q><em>vrij</em></q> is, staan in het document <q><cite>Debian
     Richtlijnen voor Vrije Software</cite></q>.
     We beloven dat het Debian systeem en al
     zijn componenten vrij zullen zijn volgens deze richtlijnen.
     Wij zullen zowel mensen die vrije als mensen die niet-vrije componenten
     maken of gebruiken op een Debian-systeem ondersteunen. We zullen ervoor zorgen dat het
     systeem nooit het gebruik van een niet-vrije component vereist.</p>
   </li>

   <li><strong>We zullen aan de vrije-software gemeenschap teruggeven</strong>
     <p>Wanneer we nieuwe delen van het Debian systeem schrijven, zullen
     we deze publiceren met een licentie die consistent is met de Debian
     Richtlijnen voor Vrije Software. We zullen het best mogelijke
     systeem maken, zodat vrije werken wijd en zijd zullen worden
     gedistribueerd en gebruikt. Oplossingen voor bugs, verbeteringen
     en verzoeken van gebruikers, zullen we terugkoppelen naar de
     <q><em>upstream</em></q>-auteurs van de componenten die in
     ons systeem zijn opgenomen.
   </li>

   <li><strong>We zullen geen problemen verbergen</strong>
     <p>We zullen ten allen tijde onze gehele database van bugrapporten
     openstellen voor publieke toegang. Rapporten die gebruikers
     online insturen zullen meteen zichtbaar zijn voor anderen.</p>
   </li>

   <li><strong>Onze prioriteiten liggen bij onze gebruikers en bij
     vrije software</strong>
     <p>We zullen ons laten leiden door de behoeften van onze gebruikers en
       de vrije-software gemeenschap. Wij zullen in onze prioriteiten hun
       belangen op de eerste plaats zetten. We zullen de behoefte van onze
       gebruikers,
       om te kunnen werken in veel verschillende computeromgevingen, ondersteunen.
       We zullen geen bezwaar maken tegen niet-vrije werken die bedoeld zijn om
       op Debian-systemen te draaien en zullen ook niet proberen, om mensen die
       zulke werken maken of gebruiken, een vergoeding te laten betalen. We zullen
       derden toelaten om distributies te ontwikkelen die zowel het Debian-systeem
       als andere werken bevatten, zonder enige betaling aan ons. Om deze doelen
       te ondersteunen, zullen we een geïntegreerd systeem van kwalitatief
       hoogwaardige materialen leveren, zonder juridische restricties, die een
       dergelijk gebruik van dit systeem zouden verhinderen.
     </p>
   </li>

   <li><strong>Werken die niet aan onze standaarden van vrije
    software voldoen</strong>
    <p>We erkennen dat sommige van onze gebruikers werken nodig hebben die niet
      voldoen aan de Debian Richtlijnen voor Vrije Software. We hebben daartoe
      in onze archieven de gebieden <q><code>contrib</code></q> en
      <q><code>non-free</code></q> gemaakt. Hoewel de pakketten in deze gebieden
      zijn geconfigureerd voor gebruik met Debian,
      zijn ze geen onderdeel van het Debian systeem. We raden cd-fabrikanten aan
      de licenties van de softwarepakketten in deze gebieden te lezen om te
      toetsen of zij deze software op hun cd's mogen verspreiden. Dus, hoewel
      niet-vrije software geen deel uitmaakt van Debian, ondersteunen we wel
      het gebruik ervan en verzorgen we de infrastructuur (zoals het
      Bug Tracking Systeem en de mailinglijsten) voor niet-vrije
      softwarepakketten.
    </p>
   </li>
</ol>
<hr />
<h2 id="guidelines">De Debian Richtlijnen voor Vrije Software (DFSG)</h2>
<ol>
   <li><p><strong>Vrije verdere verdeling</strong></p>
     <p>De licentie van een onderdeel van Debian mag niemand verbieden de
     software als een deel van een softwaredistributie met programma's van
     verschillende bronnen te verkopen of weg te geven. De licentie mag
     geen vergoedingen of andere vorm van betaling vereisen voor een
     dergelijke verkoop.</p>
   </li>
   <li><p><strong>Broncode</strong></p>
     <p>Het programma moet de broncode omvatten en verspreiding
     van zowel de broncode als het programma in gecompileerde vorm moet
     toegestaan zijn.</p>
   </li>
   <li><p><strong>Afgeleide werken</strong></p>
     <p>De licentie moet wijzigingen en afgeleide werken toestaan en
     moet toestaan dat deze worden verspreid onder dezelfde
     voorwaarden als de licentie van de originele software.</p>
   </li>
   <li><p><strong>Integriteit van de broncode van de auteur</strong></p>
     <p>De licentie mag <strong>enkel</strong> restricties opleggen aan
     verspreiding van gewijzigde broncode als de licentie toestaat dat
     zogenaamde <q><tt>patch-bestanden</tt></q>, die het doel hebben het
     programma voor compilatie te wijzigen, samen met de broncode verspreid
     mogen worden. De licentie moet expliciet de distributie toelaten van
     software die met gewijzigde broncode gecompileerd werd. De licentie mag
     eisen dat afgeleide werken een andere naam of een ander versienummer
     dragen dan de originele software. (<em>Dit is een compromis. De Debian
     groep moedigt alle auteurs aan om het wijzigen van bestanden, broncode
     of gecompileerd, niet te beperken.</em>)</p>
   </li>
   <li><p><strong>Geen discriminatie van personen of groepen</strong></p>
     <p>De licentie mag geen enkele persoon of groep van personen
     discrimineren.</p>
   </li>
   <li><p><strong>Geen onderscheid tussen toepassingsgebieden</strong></p>
     <p>De licentie mag het gebruik van het programma in een bepaald
     toepassingsgebied niet inperken. Zij mag bijvoorbeeld niet
     verbieden dat het programma door een bedrijf wordt
     gebruikt, of dat het voor genetisch onderzoek wordt ingezet.</p>
   </li>
   <li><p><strong>Verspreiding van de licentie</strong></p>
     <p>De rechten die bij het programma horen moeten gelden voor
     iedereen aan wie het wordt verspreid, zonder dat die personen nog
     aan extra voorwaarden moeten voldoen.</p>
   </li>
   <li><p><strong>De licentie mag niet specifiek voor Debian zijn</strong></p>
     <p>De rechten die bij het programma horen mogen niet afhangen van
     het feit of het programma al dan niet deel uitmaakt van een
     Debian-systeem. Als het programma uit Debian wordt gehaald en wordt
     gebruikt of verspreid zonder Debian maar verder wel binnen de
     voorwaarden van de licentie, moet iedereen aan wie het programma
     wordt verspreid dezelfde rechten hebben als wanneer het programma
     deel is van een Debian-systeem.</p>
   </li>
   <li><p><strong>De licentie mag andere software niet besmetten</strong></p>
     <p>De licentie mag geen voorwaarden opleggen aan andere software
     die samen met de gelicentieerde software wordt verspreid.
     Bijvoorbeeld, de licentie mag niet eisen dat alle andere
     programma's die op hetzelfde medium worden verspreid, vrije
     software moet zijn.</p>
   </li>
   <li><p><strong>Voorbeelden van licenties</strong></p>
     <p>De <q><strong><a href="https://www.gnu.org/copyleft/gpl.html">GPL</a></strong></q> (zie ook de <a href="http://users.skynet.be/xterm/gpld.txt">niet-officiële vertaling naar het Nederlands</a>),
     de <q><strong><a href="https://opensource.org/licenses/BSD-3-Clause">BSD</a></strong></q> en
     de <q><strong><a href="https://perldoc.perl.org/perlartistic.html">Artistic</a></strong></q>
     licentie zijn voorbeelden van licenties die wij als
     <q><em>vrij</em></q> beschouwen.
   </li>
</ol>

<p><em>Het idee om ons <q>sociaal contract met de vrije-software gemeenschap</q>
expliciet op te schrijven, werd voorgesteld door Ean Schuessler. Bruce Perens
schreef een eerste ontwerp dat in juni 1997 tijdens een e-mailconferentie
van een volle maand bijgeschaafd werd door de andere ontwikkelaars van
Debian en dat uiteindelijk werd
<a href="https://lists.debian.org/debian-announce/debian-announce-1997/msg00017.html">
aangenomen</a> als het officiële beleid van het Debian Project.</em></p>

<p><em>Bruce Perens heeft later de Debian-specifieke delen van de
Debian Richtlijnen voor Vrije Software verwijderd om de
<a href="https://opensource.org/docs/definition.php"><q>Open
Source Definitie</q></a></em> te maken.</p>

<p><em>Andere organisaties mogen afleidingen maken van dit document
en/of erop voortbouwen. Vermeld alstublieft het Debian-project als u dit
doet.</em></p>
