#use wml::debian::template title="Debian 8 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a61db9f0be033cc72386226e5214eaad477eb66e"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian-beveiligingsteam werkt in de stabiele release de pakketten bij
waarin het problemen in verband met veiligheid heeft geïdentificeerd.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina’s</a> voor
informatie over vastgestelde beveiligingsproblemen in <q>Jessie</q>.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ jessie/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Over het algemeen worden deze
aangeduid als tussenreleases.</p>

<ul>
  <li>De eerste tussenrelease, 8.1, werd uitgebracht op
      <a href="$(HOME)/News/2015/20150606">6 juni 2015</a>.</li>
  <li>De tweede tussenrelease, 8.2, werd uitgebracht op
      <a href="$(HOME)/News/2015/20150905">5 september 2015</a>.</li>
  <li>De derde tussenrelease, 8.3, werd uitgebracht op
      <a href="$(HOME)/News/2016/20160123">23 januari 2016</a>.</li>
  <li>De vierde tussenrelease, 8.4 werd uitgebracht op
      <A href="$(HOME)/News/2016/20160402">2 april 2016</a>.</li>
  <li>De vijfde tussenrelease, 8.5 werd uitgebracht op
      <A href="$(HOME)/News/2016/20160604">4 juni 2016</a>.</li>
  <li>De zesde tussenrelease, 8.6 werd uitgebracht op
      <A href="$(HOME)/News/2016/20160917">17 september 2016</a>.</li>
  <li>De zevende tussenrelease, 8.7 werd uitgebracht op
      <A href="$(HOME)/News/2017/20170114">14 januari 2017</a>.</li>
  <li>De achtste tussenrelease, 8.8 werd uitgebracht op
      <A href="$(HOME)/News/2017/20170506">6 mei 2017</a>.</li>
  <li>De negende tussenrelease, 8.9 werd uitgebracht op
      <A href="$(HOME)/News/2017/2017072202">22 juli 2017</a>.</li>
  <li>De tiende tussenrelease, 8.10 werd uitgebracht op
      <a href="$(HOME)/News/2017/20171209">9 december 2017</a>.</li>
 <li>De elfde tussenrelease, 8.11 werd uitgebracht op
       <a href="$(HOME)/News/2018/20180623">23 juni 2018</a>,</li>
</ul>

<ifeq <current_release_jessie> 8.0 "

<p>Er zijn nog geen tussenreleases voor Debian 8.</p>" "

<p>Raadpleeg de <a
href=http://http.us.debian.org/debian/dists/jessie/ChangeLog>\
ChangeLog</a>
voor details over wijzigingen tussen 8.0 en <current_release_jessie/>.</p>"/>


<p>Probleemoplossingen voor de uitgebrachte stabiele distributie gaan dikwijls
door een uitgebreide testperiode voordat ze in het archief worden aanvaard.
Nochtans zijn deze probleemoplossingen beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/jessie-proposed-updates/">\
dists/jessie-proposed-updates</a> van elke Debian-archiefspiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \#  voorgestelde updates voor een tussenrelease van 8
  deb http://ftp.us.debian.org/debian jessie-proposed-updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Zie voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
