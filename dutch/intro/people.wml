#use wml::debian::template title="De mensen: wie we zijn en wat we doen"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Hoe het allemaal begon</a>
    <li><a href="#devcont">Ontwikkelaars en medewerkers</a>
    <li><a href="#supporters">Individuen en organisaties die Debian steunen</a>
    <li><a href="#users">Gebruikers van Debian</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Omdat veel mensen het gevraagd hebben: Debian wordt uitgesproken als <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> Het is genoemd naar de maker van Debian, Ian Murdock, en zijn vrouw, Debra.</p>
</aside>

<h2><a id="history">Hoe het allemaal begon</a></h2>

<p>Het was augustus 1993 toen Ian Murdock begon te werken aan een nieuw
besturingssysteem dat openlijk zou worden gemaakt, in de geest van Linux en GNU.
Hij stuurde een open uitnodiging naar andere softwareontwikkelaars en vroeg hen
om bij te dragen aan een softwaredistributie op basis van de toen nog relatief
nieuwe Linux-kernel. Het was de bedoeling om Debian zorgvuldig en nauwgezet
samen te stellen, en met dezelfde zorg te onderhouden en te ondersteunen,
steunend op een open ontwerp, open bijdragen en met ondersteuning van de
vrije-softwaregemeenschap.</p>

<p>
Het begon als een kleine, hechte groep vrije-softwareontwikkelaars en groeide
geleidelijk uit tot een grote, goed georganiseerde gemeenschap van
ontwikkelaars, medewerkers en gebruikers.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">De volledige geschiedenis lezen</a></button></p>

<h2><a id="devcont">Ontwikkelaars en medewerkers</a></h2>

<p>
Debian is een organisatie die volledig uit vrijwilligers bestaat. Meer dan
duizend ontwikkelaars, verspreid <a href="$(DEVEL)/developers.loc">over de hele
wereld</a> werken in hun vrije tijd aan Debian. Weinigen onder ons hebben
elkaar persoonlijk ontmoet. In plaats daarvan communiceren we voornamelijk via
e-mail (mailinglijsten op <a href="https://lists.debian.org/">lists.debian.org</a>)
en IRC (kanaal #debian op irc.debian.org).
</p>

<p>
De volledige lijst van officiële leden van Debian is te vinden op
<a href="https://nm.debian.org/members">nm.debian.org</a>, en
<a href="https://contributors.debian.org">contributors.debian.org</a>
toont een lijst van alle medewerkers en teams die aan de Debian-distributie
werken.</p>

<p>Het Debian Project heeft een zorgvuldig uitgebouwde
<a href="organization">organisatiestructuur</a>. Voor meer informatie over hoe
Debian er van binnenuit uitziet, kunt u eens
rondkijken in de <a href="$(DEVEL)/">hoek voor ontwikkelaars</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Over onze filosofie lezen</a></button></p>

<h2><a id="supporters">Individuen en organisaties die Debian steunen</a></h2>

<p>Naast ontwikkelaars en medewerkers zijn er nog vele andere individuen en organisaties die deel uitmaken van de Debian-gemeenschap:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting- en hardwaresponsors</a></li>
  <li><a href="../mirror/sponsors">Spiegelserversponsors</a></li>
  <li><a href="../partners/">Ontwikkelings- en servicepartners</a></li>
  <li><a href="../consultants">Consultants</a></li>
  <li><a href="../CD/vendors">Verkopers van Debian-installatiemedia</a></li>
  <li><a href="../distrib/pre-installed">Computerverkopers die Debian vooraf installeren</a></li>
  <li><a href="../events/merchandise">Verkopers van koopwaar</a></li>
</ul>

<h2><a id="users">Gebruikers van Debian</a></h2>

<p>
Debian wordt gebruikt door een grote verscheidenheid van organisaties, groot en
klein, net als door vele duizenden individuen. Op onze pagina
<a href="../users/">Wie gebruikt Debian?</a> vindt u een lijst van educatieve,
commerciële en non-profitorganisaties en overheidsinstellingen die een korte
beschrijving hebben gegeven van hoe en waarom ze Debian gebruiken.
</p>

