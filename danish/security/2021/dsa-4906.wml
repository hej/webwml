#use wml::debian::translation-check translation="222fe2a8c3db7e4bec94b5d9a4a285666247753c" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i webbrowseren chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21201">CVE-2021-21201</a>

    <p>Gengming Liu og Jianyu Chen opdagede et problem med anvendelse efter 
    frigivelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21202">CVE-2021-21202</a>

    <p>David Erceg opdagede et problem med anvendelse efter frigivelse i 
    udvidelser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21203">CVE-2021-21203</a>

    <p>asnine opdagede et problem med anvendelse efter frigivelse i 
    Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21204">CVE-2021-21204</a>

    <p>Tsai-Simek, Jeanette Ulloa og Emily Voigtlander opdagede et problem med 
    anvendelse efter frigivelse i Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21205">CVE-2021-21205</a>

    <p>Alison Huffman opdagede en fejl i forbindelse med håndhævelse af 
    policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21207">CVE-2021-21207</a>

    <p>koocola og Nan Wang opdagede et problem med anvendelse efter frigivelse i 
    den indekserede database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21208">CVE-2021-21208</a>

    <p>Ahmed Elsobky opdagede en datavalideringsfejl i 
    QR-kode-scanneren.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21209">CVE-2021-21209</a>

    <p>Tom Van Goethem opdagede en implementeringsfejl i Storage API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21210">CVE-2021-21210</a>

    <p>@bananabr opdagede en fejl i netværksimplementeringen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21211">CVE-2021-21211</a>

    <p>Akash Labade opdagede en fejl i navigeringsimplementeringen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21212">CVE-2021-21212</a>

    <p>Hugo Hue og Sze Yui Chau opdagede en fejl i brugergrænsefladen til 
    netværksopsætning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21213">CVE-2021-21213</a>

    <p>raven opdagede et problem med anvendelse efter frigivelse i 
    implementeringen af WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21214">CVE-2021-21214</a>

    <p>Et problem med anvendelse efter frigivelse blev opdaget i 
    netværksimplementeringen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21215">CVE-2021-21215</a>

    <p>Abdulrahman Alqabandi opdagede en fejl i 
    Autofill-funktionaliteten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21216">CVE-2021-21216</a>

    <p>Abdulrahman Alqabandi opdagede en fejl i 
    Autofill-funktionaliteten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21217">CVE-2021-21217</a>

    <p>Zhou Aiting opdagede anvendelse af uinitialiseret hukommelse i 
    biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21218">CVE-2021-21218</a>

    <p>Zhou Aiting opdagede anvendelse af uinitialiseret hukommelse i 
    biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21219">CVE-2021-21219</a>

    <p>Zhou Aiting opdagede anvendelse af uinitialiseret hukommelse i 
    biblioteket pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21221">CVE-2021-21221</a>

    <p>Guang Gong opdagede utilstrækkelig validering af inddata, der ikke er 
    tillid til.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21222">CVE-2021-21222</a>

    <p>Guang Gong opdagede et bufferoverløbsproblem i JavaScript-biblioteket 
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21223">CVE-2021-21223</a>

    <p>Guang Gong opdagede et heltalsoverløbsproblem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21224">CVE-2021-21224</a>

    <p>Jose Martinez opdagede en typefejl i JavaScript-biblioteket v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21225">CVE-2021-21225</a>

    <p>Brendon Tiszka opdagede et problem med tilgang til hukommelse udenfor 
    grænserne i JavaScript-biblioteket v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21226">CVE-2021-21226</a>

    <p>Brendon Tiszka opdagede et problem med anvendelse efter frigivelse i 
    netværksimplementeringen.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 90.0.4430.85-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine chromium-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende chromium, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4906.data"
