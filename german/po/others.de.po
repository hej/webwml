# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Jens Seidel <tux-master@web.de>, 2004.
# Dr. Tobias Quathamer <toddy@debian.org>, 2005, 2011, 2012, 2017.
# Holger Wansing <linux@wansing-online.de>, 2011, 2015, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml other\n"
"PO-Revision-Date: 2020-02-16 20:19+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Ecke für Neue Mitglieder"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Schritt 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Schritt 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Schritt 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Schritt 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Schritt 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Schritt 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Schritt 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Prüfliste für Bewerber"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Siehe <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/"
"index.fr.html</a> (nur auf Französisch verfügbar) für weitere Informationen."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Weitere Informationen"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Siehe <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/"
"</a> (nur auf Spanisch verfügbar) für weitere Informationen."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adresse"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produkte"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "T-Shirts"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "Mützen"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "Aufkleber"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "Tassen"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "andere Bekleidung"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "Polo-Shirts"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "Frisbees"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "Maus-Pads"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "Abzeichen"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "Basketball-Körbe"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "Ohrringe"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "Koffer"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "Regenschirme"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "Kissenbezüge"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "Schlüsselanhänger"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Schweizer Messer"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USB-Sticks"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "Schlüsselanhänger"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "Anderes"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Verfügbare Sprachen:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Lieferung ins Ausland:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "innerhalb Europas"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Ursprungsland:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Spendet Geld an Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""
"Für die Organisation von lokalen Veranstaltungen für Freie Software wird "
"Geld zur Verfügung gestellt"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Mit&nbsp;»Debian«"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Ohne&nbsp;»Debian«"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Gekapseltes Postscript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (Mini-Button)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "identisch wie oben"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Wie lange benutzen Sie Debian bereits?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Sind Sie ein Debian-Entwickler?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Mit welchen Bereichen von Debian befassen Sie sich?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Wodurch wurde Ihr Interesse geweckt, mit Debian zu arbeiten?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Haben Sie Tipps für Frauen, die Interesse daran haben, sich näher mit Debian "
"zu beschäftigen?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Sind Sie an anderen Gruppen beteiligt, die Frauen und Technik zum Thema "
"haben? Wenn ja, welche?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Etwas mehr über Sie ..."

#~ msgid "Where:"
#~ msgstr "Wo:"

#~ msgid "Specifications:"
#~ msgstr "Spezifikationen:"

#~ msgid "Architecture:"
#~ msgstr "Architektur:"

#~ msgid "Who:"
#~ msgstr "Wer:"

#~ msgid "Wanted:"
#~ msgstr "Benötigt:"

#~ msgid "URL"
#~ msgstr "Links"

#~ msgid "Version"
#~ msgstr "Version"

#~ msgid "Status"
#~ msgstr "Status"

#~ msgid "Package"
#~ msgstr "Paket"

#~ msgid "ALL"
#~ msgstr "ALLE"

#~ msgid "Unknown"
#~ msgstr "unbekannt"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "nicht OK?"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD"
#~ msgstr "nicht OK"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Old banner ads"
#~ msgstr "Alte Bannerwerbung"

#~ msgid "Download"
#~ msgstr "Herunterladen"

#~ msgid "Unavailable"
#~ msgstr "Nicht verfügbar"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "Unbekannt"

#~ msgid "No images"
#~ msgstr "Keine Images"

#~ msgid "No kernel"
#~ msgstr "Kein Kernel"

#~ msgid "Not yet"
#~ msgstr "Noch nicht"

#~ msgid "Building"
#~ msgstr "Im Bau"

#~ msgid "Booting"
#~ msgstr "Startet"

#~ msgid "sarge (broken)"
#~ msgstr "Sarge (defekt)"

#~ msgid "sarge"
#~ msgstr "Sarge"

#~ msgid "Working"
#~ msgstr "Funktionstüchtig"
