# From: r.meier@siemens.com
#use wml::debian::translation-check translation="35ec142303f1c31f891a4e535ddb663898549959"
# Initial Translation: Helge Kreutzmann <kreutzm@itp.uni-hannover.de> 2005-09-20
# Webpage URL is usually redirected, this is normal, Roger Meier 2005-09-20
# New translation provided by r.meier@siemens.com

<define-tag pagetitle>Siemens</define-tag>
<define-tag webpage>https://www.siemens.com/buildingtechnologies/</define-tag>

#use wml::debian::users

<p>
  Siemens nutzt Debian in vielen Bereichen wie Forschung, Entwicklung, und 
  Infrastruktur sowie in unseren Produkten.
</p>
<p>    
  Debian ist die Basis von vielen Produkten und Services in den Bereichen 
  Automatisierung und Digitalisierung von Prozess und Produktionsindustrien, 
  intelligenten Mobilitätslösungen für Straße und Eisenbahn, Gesundheitswesen 
  und Medizinaltechnik, sowie verteilten Energiesystemen und intelligenter
  Infrastruktur für Gebäude.  
</p>
<p>
  Debian ist beispielsweise Teil der  
  <a href="https://www.siemens-healthineers.com/magnetic-resonance-imaging">Siemens MRI-Geräte</a>,
  befindet sich in modernen Siemens Zügen und Metro Systemen, im Bereich 
  <a href="https://press.siemens.com/global/de/feature/siemens-digitalisiert-die-infrastruktur-des-norwegischen-bahnnetzes">Schienen und Bahnhöfe</a>, 
  und in vielen weiteren Geräten. Im weiteren ist Siemens Gründungsmitglied der 
  <a href="https://www.cip-project.org/">Civil Infrastructure Platform</a>,
  welche Debian LTS as Basis einsetzt.
</p>
<p>    
  Selbstverständlich ist Debian auch ein essentieller Bestandteil der 
  Serverinfrastruktur. Auf unseren Desktoprechner setzen wir häufig Debian ein 
  wenn ein Linux-System benötigt wird; zum Beispiel werden die meisten 
  Linux-basierten Produkte im Gebäudetechnikbereich auf Debian entwickelt. 
  Siemens bietet auch Debian-basierte embedded Distributionen an, so wie 
  <a href="https://siemens.com/embedded">Sokol™ Omni OS</a>.
</p>
<p>    
  Wieso nutzen wir Debian: Die hohe Qualität und Stabilität die wir mit Debian
  erreichen ist großartig! Die Verfügbarkeit von Softwareaktualisierungen und
  Behandlung von Sicherheitsproblemen is einzigartig und die Anzahle verfügbarer 
  Softwarepakete ist unschlagbar.
</p>
