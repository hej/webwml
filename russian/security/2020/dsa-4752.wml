#use wml::debian::translation-check translation="5346ad19e1bb39a2123f70e49de6fe4ffa9caa5b" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В BIND, реализации DNS-сервера, было обнаружено несколько
уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8619">CVE-2020-8619</a>

    <p>Было обнаружено, что символ звёздочки в пустом нетерминальном выражении
    может вызывать ошибку утверждения, что приводит к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

    <p>Дэйв Фельдман, Джеф Уоррен и Джоэль Куннингэм сообщили, что
    сокращённый TSIG-запрос может вызывать ошибку утверждения, что приводит
    к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

    <p>Лю Чю сообщил, что уязвимость в коде PKCS#11 может приводить к ошибке
    утверждения, которая может вызываться удалённо, что приводит к отказу
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8624">CVE-2020-8624</a>

    <p>Джуп Бунен сообщил, что правила обновления политики типа <q>subdomain</q>
    используются неправильно, что позволяет обновлять все части зоны вместе
    с тем поддоменом, обновление которого запланировано.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:9.11.5.P4+dfsg-5.1+deb10u2.</p>

<p>Рекомендуется обновить пакеты bind9.</p>

<p>С подробным статусом поддержки безопасности bind9 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4752.data"
