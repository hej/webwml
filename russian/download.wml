#use wml::debian::template title="Спасибо за загрузку Debian!"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="Lev Lamberov"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>Это Debian <:=substr '<current_initial_release>', 0, 2:>, кодовое имя <em><current_release_name></em>, образ для сетевой установки для архитектуры <: print $arches{'amd64'}; :>.</p>

<p>Если загрузка не началась автоматически, то нажмите на <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Контрольная сумма: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Подпись</a></p>



<div class="tip">
	<p><strong>Важно</strong>: обязательно <a href="$(HOME)/CD/verify">проверьте контрольную сумму загруженного файла</a>.</p>
</div>

<p>ISO-образы установщика Debian являются гибридными образами. Это означает, что их можно записать на компакт-диск/DVD/BD ИЛИ на <a href="https://www.debian.org/CD/faq/#write-usb">USB-носитель</a>.</p>

<h2 id="h2-1">
	Другие образы</h2>

<p>Другие образы, такие как живые системы, автономные установщики для систем без сетевого подключения, установщики для других архитектур ЦП, а также образы для облачных окружений можно найти на странице <a href="$(HOME)/distrib/">Где взять Debian</a>.</p>

<p>Неофициальные образы с <a href="https://wiki.debian.org/Firmware"><strong>несвободными прошивками</strong></a>, полезные для некоторых сетевых и графических адаптеров, можно загрузить со страницы <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Неофициальные несвободные образы, содержащие пакеты с прошивками</a>.</p>

<h2 id="h2-2">Связанные ссылки</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Руководство по установке</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Информация о выпуске</a></p>
<p><a href="$(HOME)/CD/verify">Руководство по проверке ISO-образа</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Альтернативные сайты загрузки</a></p>
<p><a href="$(HOME)/releases">Другие выпуски</a></p>
