<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Chen Zhaojun of Alibaba Cloud Security Team discovered a critical security
vulnerability in Apache Log4j, a popular Logging Framework for Java. JNDI
features used in configuration, log messages, and parameters do not protect
against attacker controlled LDAP and other JNDI related endpoints. An attacker
who can control log messages or log message parameters can execute arbitrary
code loaded from LDAP servers when message lookup substitution is enabled. From
version 2.15.0, this behavior has been disabled by default.</p>

<p>This update also fixes <a href="https://security-tracker.debian.org/tracker/CVE-2020-9488">\
CVE-2020-9488</a> in the oldstable distribution (buster). Improper validation
of certificate with host mismatch in Apache Log4j SMTP appender. This could
allow an SMTPS connection to be intercepted by a man-in-the-middle attack
which could leak any log messages sent through that appender.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.15.0-1~deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.15.0-1~deb11u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5020.data"
# $Id: $
