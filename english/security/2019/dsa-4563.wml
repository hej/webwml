<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>These vulnerabilities have been discovered in the webkit2gtk web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8812">CVE-2019-8812</a>

    <p>An anonymous researcher discovered that maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8814">CVE-2019-8814</a>

    <p>Cheolung Lee discovered that maliciously crafted web content may
    lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.26.2-1~deb10+1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4563.data"
# $Id: $
