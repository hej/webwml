<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities were found in cURL, an URL transfer library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000120">CVE-2018-1000120</a>

    <p>Duy Phan Thanh reported that curl could be fooled into writing a zero byte
    out of bounds when curl was told to work on an FTP URL, with the setting to
    only issue a single CWD command. The issue could be triggered if the
    directory part of the URL contained a "%00" sequence.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000121">CVE-2018-1000121</a>

    <p>Dario Weisser reported that curl might dereference a near-NULL address when
    getting an LDAP URL. A malicious server that sends a particularly crafted
    response could made crash applications that allowed LDAP URL relying on
    libcurl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000122">CVE-2018-1000122</a>

    <p>OSS-fuzz and Max Dymond found that curl can be tricked into copying data
    beyond the end of its heap based buffer when asked to transfer an RTSP URL.
    curl could calculate a wrong data length to copy from the read buffer.
    This could lead to information leakage or a denial of service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.26.0-1+wheezy25.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1309.data"
# $Id: $
