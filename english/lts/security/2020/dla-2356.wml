<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilites have been reported against FreeRDP, an Open Source
server and client implementation of the Microsoft RDP protocol.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0791">CVE-2014-0791</a>

    <p>An integer overflow in the license_read_scope_list function in
    libfreerdp/core/license.c in FreeRDP allowed remote RDP
    servers to cause a denial of service (application crash) or possibly
    have unspecified other impact via a large ScopeCount value in a Scope
    List in a Server License Request packet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11042">CVE-2020-11042</a>

    <p>In FreeRDP there was an out-of-bounds read in update_read_icon_info.
    It allowed reading an attacker-defined amount of client memory (32bit
    unsigned -> 4GB) to an intermediate buffer. This could have been used
    to crash the client or store information for later retrieval.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11045">CVE-2020-11045</a>

    <p>In FreeRDP there was an out-of-bound read in in
    update_read_bitmap_data that allowed client memory to be read to an
    image buffer. The result displayed on screen as colour.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11046">CVE-2020-11046</a>

    <p>In FreeRDP there was a stream out-of-bounds seek in
    update_read_synchronize that could have lead to a later out-of-bounds
    read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11048">CVE-2020-11048</a>

    <p>In FreeRDP there was an out-of-bounds read. It only allowed to abort
    a session. No data extraction was possible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11058">CVE-2020-11058</a>

    <p>In FreeRDP, a stream out-of-bounds seek in
    rdp_read_font_capability_set could have lead to a later out-of-bounds
    read. As a result, a manipulated client or server might have forced a
    disconnect due to an invalid data read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11521">CVE-2020-11521</a>

    <p>libfreerdp/codec/planar.c in FreeRDP had an Out-of-bounds Write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11522">CVE-2020-11522</a>

    <p>libfreerdp/gdi/gdi.c in FreeRDP had an Out-of-bounds Read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11523">CVE-2020-11523</a>

    <p>libfreerdp/gdi/region.c in FreeRDP had an Integer Overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11525">CVE-2020-11525</a>

    <p>libfreerdp/cache/bitmap.c in FreeRDP had an Out of bounds read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11526">CVE-2020-11526</a>

    <p>libfreerdp/core/update.c in FreeRDP had an Out-of-bounds Read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13396">CVE-2020-13396</a>

    <p>An out-of-bounds (OOB) read vulnerability has been detected in
    ntlm_read_ChallengeMessage in
    winpr/libwinpr/sspi/NTLM/ntlm_message.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13397">CVE-2020-13397</a>

    <p>An out-of-bounds (OOB) read vulnerability has been detected in
    security_fips_decrypt in libfreerdp/core/security.c due to an
    uninitialized value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13398">CVE-2020-13398</a>

    <p>An out-of-bounds (OOB) write vulnerability has been detected in
    crypto_rsa_common in libfreerdp/crypto/crypto.c.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.1.0~git20140921.1.440916e+dfsg1-13+deb9u4.</p>

<p>We recommend that you upgrade your freerdp packages.</p>

<p>For the detailed security status of freerdp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/freerdp">https://security-tracker.debian.org/tracker/freerdp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2356.data"
# $Id: $
