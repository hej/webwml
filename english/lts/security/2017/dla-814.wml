<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in OpenSSL:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7056">CVE-2016-7056</a>

    <p>A local timing attack was discovered against ECDSA P-256.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8610">CVE-2016-8610</a>

    <p>It was discovered that no limit was imposed on alert packets during
    an SSL handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3731">CVE-2017-3731</a>

    <p>Robert Swiecki discovered that the RC4-MD5 cipher when running on
    32 bit systems could be forced into an out-of-bounds read, resulting
    in denial of service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.1t-1+deb7u2.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-814.data"
# $Id: $
