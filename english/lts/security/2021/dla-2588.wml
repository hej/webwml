<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been detected in zeromq3.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20234">CVE-2021-20234</a>

    <p>Memory leak in client induced by malicious server(s) without CURVE/ZAP.</p>

    <p>From issue description [<a href="https://github.com/zeromq/libzmq/security/advisories/GHSA-wfr2-29gj-5w87">1</a>].
    When a pipe processes a delimiter and is already not in active state but
    still has an unfinished message, the message is leaked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20235">CVE-2021-20235</a>

    <p>Heap overflow when receiving malformed ZMTP v1 packets.</p>

    <p>From issue description [<a href="https://github.com/zeromq/libzmq/security/advisories/GHSA-fc3w-qxf5-7hp6">2</a>].
    The static allocator was implemented to shrink its recorded size similarly
    to the shared allocator. But it does not need to, and it should not,
    because unlike the shared one the static allocator always uses a static
    buffer, with a size defined by the ZMQ_IN_BATCH_SIZE socket option
    (default 8192), so changing the size opens the library to heap overflows.
    The static allocator is used only with ZMTP v1 peers.</p>

</li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.2.1-4+deb9u4.</p>

<p>We recommend that you upgrade your zeromq3 packages.</p>

<p>For the detailed security status of zeromq3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zeromq3">https://security-tracker.debian.org/tracker/zeromq3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2588.data"
# $Id: $
