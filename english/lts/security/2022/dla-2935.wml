<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Expat, an XML parsing C
library, which could result in denial of service or potentially the
execution of arbitrary code, if a malformed XML file is processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.2.0-2+deb9u5.</p>

<p>We recommend that you upgrade your expat packages.</p>

<p>For the detailed security status of expat please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/expat">https://security-tracker.debian.org/tracker/expat</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2935.data"
# $Id: $
