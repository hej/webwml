<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the Django Python web development framework did not correct identify HTTP connections when a reverse proxy connected via HTTPS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12781">CVE-2019-12781: Incorrect HTTP detection with reverse-proxy connecting via HTTPS</a>

    <p>When deployed behind a reverse-proxy connecting to Django via HTTPS, <tt>django.http.HttpRequest.scheme</tt> would incorrectly detect client requests made via HTTP as using HTTPS. This entails incorrect results for <tt>is_secure()</tt>, and <tt>build_absolute_uri()</tt>, and that HTTP requests would not be redirected to HTTPS in accordance with <tt>SECURE_SSL_REDIRECT</tt>.</p>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u6.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1842.data"
# $Id: $
