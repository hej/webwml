#use wml::debian::translation-check translation="0cc922cd661d77cfeed7f482bce1cfba75c197ae" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service, ou
une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

<p>Sur les processeurs Intel prenant en charge la virtualisation matérielle
avec des tables de pagination (« Extended Page Tables » – EPT), une machine
virtuelle peut manipuler la gestion de mémoire matérielle pour provoquer
une erreur de « Machine Check » (MCE) et un déni de service (blocage ou
plantage).</p>

<p>Le client déclenche cette erreur en modifiant les tables de pagination
sans vidage de la TLB, si bien qu'à la fois des entrées de 4 Ko et 2 Mo
sont chargées pour la même adresse virtuelle dans l'instruction TLB (iTLB).
Cette mise à jour implémente une mitigation dans KVM qui empêche les
machines virtuelles clientes de charger des entrées de 2 Mo dans iTLB. Cela
réduira la performance des machines virtuelles clientes.</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
dans les paquet linux-doc-4.9 ou linux-doc-4.19.</p>

<p>Une mise à jour de qemu, ajoutant la prise en charge de la fonctionnalité
PSCHANGE_MC_NO qui permet de désactiver les mitigations iTLB Multihit dans
les hyperviseurs imbriqués, sera fournie par la DSA 4566-1.</p>

<p>L'explication par Intel de ce problème est disponible sur la page
<url "https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

<p>Intel a découvert que leurs processeurs graphiques de huitième et
neuvième génération, lorsqu'ils lisent certains registres tandis que le
processeur graphique est dans un état de faible puissance, peuvent
provoquer un blocage système. Un utilisateur local autorisé à utiliser le
processeur graphique peut se servir de cela pour un déni de service.</p>

<p>Cette mise à jour atténue le problème grâce à des modifications dans le
pilote des cartes graphiques i915.</p>

<p>Les puces affectées (de huitième et neuvième génération) sont listées
sur la page
<url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

<p>Intel a découvert que les processeurs graphiques de neuvième génération
et plus récents manquent de vérification de sécurité dans le « Blitter
Command Streamer » (BCS). Un utilisateur local autorisé à utiliser le
processeur graphique pourrait se servir de cela pour accéder à toute la
mémoire à laquelle le processeur graphique a accès, ce qui pourrait avoir
pour conséquence un déni de service (corruption de mémoire ou plantage), la
divulgation d'informations sensible ou une élévation des privilèges.</p>

<p>Cette mise à jour atténue le problème en ajoutant une vérification de
sécurité au pilote i915.</p>

<p>Les puces affectées (à partir de la neuvième génération) sont listées
sur la page
<url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

<p>Sur les processeurs Intel prenant en charge la mémoire transactionnelle
(TSX), une transaction qui va être interrompue peut être poursuivie pour
une exécution spéculative, lisant des données sensibles à partir des
tampons internes et les divulguant à travers les opérations dépendantes.
Intel appelle cela <q>TSX Asynchronous Abort</q> (TAA).</p>

<p>Pour les processeurs affectés par les problèmes «  Microarchitectural
Data Sampling (MDS) » précédemment publiés
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>),
la mitigation existante atténue aussi ce problème.</p>

<p>Pour les processeurs qui sont vulnérables aux TAA mais pas aux MDS,
cette mise à jour désactive TSX par défaut. Cette mitigation requiert un
microcode mis à jour du processeur. Un paquet mis à jour du paquet
intel-microcode (disponible seulement dans Debian non-free) sera fournit au
moyen de la DSA 4565-1. Le microcode mis à jour du processeur peut aussi être
disponible dans le cadre de la mise à jour du microprogramme du système
(« BIOS »).</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">
ou dans les paquets linux-doc-4.9 ou linux-doc-4.19.</p>

<p>L'explication par Intel de ce problème est disponible sur la page
<url "https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 4.9.189-3+deb9u2.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.67-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4564.data"
# $Id: $
