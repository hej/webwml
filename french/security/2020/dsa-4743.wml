#use wml::debian::translation-check translation="ea4695cc27694f8fc80eb1e6329c9fd24548d384" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans ruby-kramdown, un analyseur et un
convertisseur Markdown rapide et pur Ruby, qui pourrait avoir pour
conséquence un accès en lecture non prévu à des fichiers ou l'exécution de
code Ruby embarqué non prévu quand l'extension &#123;::options /&#125; est
utilisée en même temps que l'option <q>template</q>.</p>

<p>La mise à jour introduit une nouvelle option
<q>forbidden_inline_options</q> pour limiter les options permises avec
l'extension &#123;::options /&#125;. Par défaut l'option <q>template</q>
est interdite.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.17.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-kramdown.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-kramdown,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-kramdown">\
https://security-tracker.debian.org/tracker/ruby-kramdown</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4743.data"
# $Id: $
