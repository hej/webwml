#use wml::debian::translation-check translation="ccbe28e935d5f926bfb38368d839bae77b408510" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que la fonction BN_mod_sqrt() d'OpenSSL
pouvait être piégée dans une boucle infinie. Cela pouvait avoir pour
conséquence un déni de service au moyen de certificats mal formés.</p>

<p>Des détails supplémentaires sont disponibles dans l'annonce amont :
<a href="https://www.openssl.org/news/secadv/20220315.txt">\
https://www.openssl.org/news/secadv/20220315.txt</a></p>

<p>En complément, cette mise à jour corrige un bogue de propagation de
retenue spécifique aux architectures MIPS.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 1.1.1d-0+deb10u8.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 1.1.1k-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5103.data"
# $Id: $
