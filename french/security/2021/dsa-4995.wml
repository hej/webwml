#use wml::debian::translation-check translation="75b99797a7e35c030fc904a603d0d4186d4c5ac4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30846">CVE-2021-30846</a>

<p>Sergei Glazunov a découvert que le traitement d'un contenu web
contrefait pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30851">CVE-2021-30851</a>

<p>Samuel Gross a découvert que le traitement d'un contenu web contrefait
pourrait conduire à l'exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42762">CVE-2021-42762</a>

<p>Un rapporteur anonyme a découvert un contournement limité du bac à sable
de Bubblewrap qui permet à un processus dans le bac à sable d'entraîner les
processus de l'hôte à penser que le processus dans le bac à sable n'est pas
confiné.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2.34.1-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.34.1-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4995.data"
# $Id: $
