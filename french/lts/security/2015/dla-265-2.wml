#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le correctif original ne désactivait pas la prise en charge de la
vérification de KDC par défaut et modifiait la signature de checkPassowrd().
Cette mise à jour corrige cela.</p>

<p>Voici le texte de l'annonce originale :</p>

<p>Martin Prpic a signalé la possibilité d'une attaque de type « homme du
milieu » dans le code de pykerberos au Bugzilla de Red Hat (le suiveur de
bogues de Fedora). Le problème original a déjà été signalé à l'amont [1].
Nous citons partiellement le rapport de bogue amont ci-dessous :</p>

<p>La méthode checkPassword() de python-kerberos était gravement non
sécurisée dans les versions précédentes. Elle exécutait (et exécute encore
par défaut) un kinit (AS-REQ) pour demander à un centre de distribution de
clés (KDC) un « Ticket Granting Ticket » (TGT) pour le commettant de
l'utilisateur donné, et interprète son succès ou son échec comme
l'indication que le mot de passe est correct ou non. Il ne vérifie pas,
néanmoins, qu'il parle vraiment à un KDC de confiance : un attaquant peut
simplement répondre plutôt avec un AS-REP qui correspond au mot de passe
qu'il vient de vous donner.</p>

<p>Imaginez que vous êtes en train de vérifier un mot de passe en utilisant
une authentification LDAP plutôt que Kerberos : vous utiliseriez, bien sûr,
TLS en liaison avec LDAP pour vous assurer que vous parlez à un serveur
LDAP de confiance véritable. La même exigence s'applique ici. Kinit n'est
pas un service de vérification de mot de passe.</p>

<p>La manière habituelle de faire est de prendre le TGT obtenu avec le mot
passe de l'utilisateur, et ensuite d'obtenir un ticket pour un commettant
dont le vérificateur possède des clés (par exemple un serveur web traitant
un formulaire de connexion nom d'utilisateur/mot de passe pourrait obtenir
un ticket pour son propre commettant HTTP/host@REALM), qu'il peut alors
vérifier. Notez que cela requiert que le vérificateur possède sa propre
identité Kerberos, ce qui est imposé par la nature symétrique de Kerberos
(tandis que dans le cas de LDAP, l'utilisation du chiffrement à clé
publique permet une vérification anonyme).</p>

<p>Avec cette version du paquet pykerberos, une nouvelle option est
introduite pour la méthode checkPassword(). Définir « verify » à « True »
lors de l'utilisation de checkPassword() réalisera une vérification de KDC.
Pour que cela fonctionne, il faut fournir un fichier krb5.keytab contenant
les clés du commettant pour le service que vous vous voulez utiliser.</p>

<p>Comme le fichier krb5.keytab par défaut dans /etc n'est normalement pas
accessible aux utilisateurs et processus qui ne sont pas superutilisateurs,
il faut s'assurer qu'un fichier krb5.keytab personnalisé contenant les
bonnes clés des commettants est fourni à l'application qui utilise la
variable d'environnement KRB5_KTNAME.</p>
<p><b>Note</b> : dans Debian Squeeze(-lts), la prise en charge de la
vérification de KDC est désactivée par défaut afin de ne pas casser les
réglages existants.</p>

<p>[1] <a href="https://www.calendarserver.org/ticket/833">\
https://www.calendarserver.org/ticket/833</a></p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version  1.1+svn4895-1+deb6u2 de pykerberos.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2015/dla-265-2.data"
# $Id: $
