#use wml::debian::translation-check translation="3de42d42753a712f0dd0effbdf52b63ada1090be" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle
d’utilisation de mémoire après libération dans icu, une bibliothèque qui
fournit une fonctionnalité Unicode et locale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21913">CVE-2020-21913</a>

<p>International Components for Unicode (ICU-20850), version 66.1, contient un
bogue d’utilisation de mémoire après libération dans la fonction
pkg_createWithAssemblyCode dans le fichier tools/pkgdata/pkgdata.cpp.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 57.1-6+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2784.data"
# $Id: $
