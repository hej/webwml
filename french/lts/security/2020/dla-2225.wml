#use wml::debian::translation-check translation="9994d542d20c7a80208ae674a6195422a9e20053" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes de gestion de mémoire ont été trouvés dans gst-plugins-good0.10,
une collection de greffons de GStreamer pour l’ensemble <q>good</q> :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10198">CVE-2016-10198</a>

<p>Une lecture non autorisée peut être déclenchée dans l’élément aacparse
à l'aide d'un fichier contrefait de manière malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5840">CVE-2017-5840</a>

<p>Une lecture hors limites de tas peut être déclenchée dans l’élément qtdemux
à l'aide d'un fichier contrefait de manière malveillante.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.10.31-3+nmu4+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-good0.10.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2225.data"
# $Id: $
