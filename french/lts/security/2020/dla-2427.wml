#use wml::debian::translation-check translation="850a5e119ed7c2318d70011ee554ef4f41e1210a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de dépassement de tampon ont été trouvées dans le
processus de décodage d’image QUIC du système d’affichage à distance SPICE avant
spice-0.14.2-1.</p>

<p>À la fois le client (spice-gtk) et le serveur de SPICE sont touchés par ces
défectuosités. Celles-ci permettent à un client ou à un serveur malveillant
d’envoyer des messages contrefaits pour l'occasion qui, lorsque traités
par l’algorithme de compression d’image de QUIC, aboutissaient à un plantage du
processus ou à une exécution potentielle de code.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.12.8-2.1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spice.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de spice, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/spice">https://security-tracker.debian.org/tracker/spice</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2427.data"
# $Id: $
