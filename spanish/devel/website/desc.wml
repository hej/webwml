#use wml::debian::template title="Cómo está hecho www.debian.org"
#use wml::debian::toc
#use wml::debian::translation-check translation="7f876037d8c7ab141d9c245fcd09a3b77e25b3a7"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Apariencia</a></li>
<li><a href="#sources">Fuentes</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Generar el sitio web</a></li>
<li><a href="#help">Cómo ayudar</a></li>
<li><a href="#faq">Cómo no ayudar... (Preguntas frecuentes)</a></li>
</ul>

<h2><a id="look">Apariencia</a></h2>

<p>El sitio web de Debian es una colección de directorios y
archivos ubidcados en <code>/org/www.debian.org/www</code>
en <em>www-master.debian.org</em>. La mayor parte
de las páginas son archivos HTML estáticos. No contienen elementos dinámicos
como scripts CGI o PHP, ya que existen réplicas del sitio web.
</p>

<p>El sitio web de Debian usa el lenguaje «Website Meta Language» 
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) para generar las páginas HTML,
incluyendo cabeceras y pies de página, tablas de contenido, etc.
Aunque un archivo <code>.wml</code> puede parecer HTML a primera vista, HTML es únicamente uno de los tipos
de información extra que puede utilizarse en WML. También se puede incluir código Perl
en una página y por tanto hacer casi cualquier cosa.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Nuestras páginas web cumplen actualmente el estándar <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.
</aside>

<p>Cuando WML ha terminado de ejecutar sus distintos filtros en un archivo,
el producto final es puro HTML. Tenga en cuenta que aunque WML comprueba 
(y algunas veces corrige «automágicamente») la validez básica del código HTML, 
debería instalar una herramienta como
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
y/o
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
como comprobador de sintaxis y de estilo.
</p>

<p>
Todo el que esté contribuyendo de manera regular al sitio web de Debian debería instalar
WML para probar el código y asegurarse de que las páginas HTML resultantes parecen correctas. Si
está ejecutando Debian, simplemente instale el paquete <code>wml</code>.
Para más información, consulte la página sobre <a href="using_wml">uso de WML</a>.
</p>

<h2><a id="sources">Fuentes</a></h2>

<p>
Usamos Git para almacenar las fuentes del sitio web de Debian. El sistema de control
de versiones nos permite mantener un registro de todos los cambios y podemos ver quién
cambió qué, cuándo e incluso por qué. Git ofrece una forma segura de controlar la
edición concurrente de archivos fuente por parte de múltiples autores, una tarea
crucial para el equipo de la web de Debian, debido a que es bastante numeroso.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Lea más acerca de Git</a></button></p>

<p>
Aquí hay más información sobre cómo se estructuran las fuentes:
</p>

<ul>
<li>El directorio superior en el repositorio Git (<code>webwml</code>) contiene
directorios con los nombres de los idiomas de las distintas páginas,
dos «Makefiles» y varios scripts. Los nombres de directorio para páginas traducidas
debería ir en inglés y minúsculas, por ejemplo <code>german</code>, no <code>Deutsch</code>.</li>

<li>El archivo <code>Makefile.common</code> es especialmente importante,
pues contiene algunas reglas comunes que son aplicables mediante la inclusión de este archivo
en otros «Makefiles».</li>

<li>Todos los subdirectorios de cada idioma también contienen «Makefiles», archivos fuente <code>.wml</code>
y subdirectorios adicionales. Los nombres de los archivos y directorios siguen un cierto patrón,
de tal manera que los enlaces funcionen para todas las páginas traducidas.
Algunos directorios también incluiyen un archivo <code>.wmlrc</code> de configuración con
órdenes adicionales y preferencias para WML.</li>

<li>El directorio <code>webwml/english/template</code> contiene archivos WML especiales que
funcionan como plantillas. Pueden ser referenciados desde todos los demás
archivos utilizando la orden <code>#use</code>.</li>
</ul>

<p>
Por favor, tenga en cuenta: con objeto de que los cambios en las plantillas se propaguen a los archivos
que las usan, los archivos dependen de ellas en los «Makefiles».
Una amplia mayoría de los archivos utiliza la plantilla <code>template</code>,
la dependencia genérica, teniendo la siguiente línea en la parte superior:
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
Por supuesto, hay excepciones a esta regla.
</p>

<h2><a id="scripts">Scripts</a></h2>

<p>
Los scripts están escritos mayormente en código de shell o en Perl. Algunos
de ellos funcionan de manera independiente y otros están integrados en
los archivos fuente de WML.
</p>

<ul>
<li><a href="https://salsa.debian.org/webmaster-team/cron.git">«webmaster-team/cron»</a>: 
este repositorio Git contiene todos los scripts que se usan para actualizar el sitio web de Debian,
es decir, las fuentes para los scripts de construcció de <code>www-master</code>.</li>
<li><a href="https://salsa.debian.org/webmaster-team/packages">«webmaster-team/packages»</a>:
este repositorio Git contien las fuentes de los scripts de actualización de <code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Generar el sitio web</a></h2>

<p>
WML, las plantillas y los scripts de shell y Perl son todos los ingredientes que se necesitan para generar el sitio web de Debian:
</p>
<ul>
  <li>La mayor parte se genera utilizando WML (desde el <a href="$(DEVEL)/website/using_git">repositorio Git</a>).</li>
  <li>La documentación se genera bien utilizando DocBook XML (<a href="$(DOC)/vcs">repositorio Git de <q>ddp</q></a>)
o con <a href="#scripts">scripts de cron</a> desde los correspondientes paquetes Debian.</li>
  <li>Algunas partes del sitio se generan mediante scripts utilizando otras fuentes,
por ejemplo las páginas de (de)suscripción de las listas de correo.</li>
</ul>

<p>
Seis veces al día se ejecuta una actualización automática (desde los repositorios Git y otras fuentes).
Aparte de esto, regularmente ejecutamos las siguiente comprobaciones sobre la totalidad del sitio web:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
Los registros actuales de construcción para el sitio web pueden
localizarse en <url "https://www-master.debian.org/build-logs/">.
</p>

<p>Si le gustaría contribuir en este sitio, <strong>no</strong> empiece
simplemente añadiendo o editando elementos en el directorio <code>www/</code>.
Por favor, contacte primero con <a href="mailto:webmaster@debian.org">los administradores de la web</a>.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span>Como nota más técnica:
todos los archivos y directorios pertenecen al grupo <code>debwww</code> que tiene permisos de escritura.
De esta forma, el equipo de la web puede modificar contenidos en el directorio web.
El modo <code>2775</code> sobre directorios significa que todos los archivos creados heredarán el grupo (<code>debwww</code>).
Se espera que los miembros del grupo establezcan <code>umask 002</code>, de forma que los archivos se creen con permisos de escritura.</p>
</aside>

<h2><a id="help">Cómo ayudar</a></h2>

<p>
Animamos a todo el mundo a ayudar con el sitio web de Debian. Si tiene
alguna información valiosa relacionada con Debian que piense que le falta a nuestras páginas,
<a href="mailto:debian-www@lists.debian.org">contacte con nosotros</a>: la veremos incluida.
También consulte los <a href="https://www-master.debian.org/build-logs/">registros de construcción</a> mencionados arriba
por si tiene sugerencias para arreglar algún fallo.
</p>

<p>
Además estamos buscando personas que puedan ayudar con el diseño
(gráficos, estructura y apariencia, etc.). Si habla inglés con fluidez,
por favor considere revisar nuestras páginas e <a href="mailto:debian-www@lists.debian.org">infórmenos</a> 
de los errores encontrados.
Si habla otra lengua, quizá quiera ayudara traducir
páginas existentes o arreglar fallos en las páginas ya traducidas.
En ambos casos, por favor consulte la lista de
<a href="translation_coordinators">coordinadores de traducción</a> y
contacte con la persona responsable. Para más información, 
vea nuestra <a href="translating">página para traductores</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Lea nuestro archivo «TODO» (lo que queda por hacer)</a></button></p>

<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Cómo no ayudar... (Preguntas frecuentes)</a></h2>

<p>
<strong>[P] Quiero incluir en el sitio web de Debian <em>este detalle vistoso</em>, ¿puedo?</strong>
</p>

<p>
[R] No. Queremos que www.debian.org sea todo lo accesible que se pueda,
por tanto
</p>
<ul>
    <li>no usamos extensiones específicas de un navegador.
    <li>no dependemos únicamente de imágenes. Las imágenes pueden utilizarse
	para clarificar, pero la información de www.debian.org tiene que
	mantenerse accesible para navegadores de web de sólo texto, como
	lynx.
</ul>

<p>
<strong>[P] Tengo una buena idea. ¿Pueden habilitar <em>esto</em> o <em>esto otro</em> en 
el servidor HTTP de www.debian.org, por favor?</strong>
</p>

<p>
[R] No. Queremos hacer la vida más fácil a los administradores de las
réplicas de www.debian.org, por tanto: «no a las características especiales
de HTTPD, por favor». No, ni siquiera SSI («Server Side Includes»). Se ha hecho una única excepción
con la negociación de contenido. Esto se debe a que es la única forma
robusta de dar servicio en múltiples idiomas.
</p>
