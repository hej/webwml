#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

    <p>Matt Delco informó de una condición de carrera en la utilidad coalesced MMIO
    de KVM que podría dar lugar a acceso fuera de límites en el núcleo.
    Un atacante local con permisos para acceder a /dev/kvm podría usar esto para
    provocar denegación de servicio (corrupción de memoria o caída) o, posiblemente,
    para elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

    <p>Peter Pi, del Tencent Blade Team, descubrió que faltaba una comprobación de límites
    en vhost_net, el controlador de backends de red para anfitriones KVM, lo que daba lugar
    a desbordamiento de memoria cuando el anfitrión iniciaba la migración en caliente de una VM.
    Un atacante con control de una VM podría usar esto para provocar denegación de
    servicio (corrupción de memoria o caída) o, posiblemente, para elevación
    de privilegios en el anfitrión.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

    <p>Hui Peng y Mathias Payer informaron de que faltaba una comprobación de límites en el
    código para analizar sintácticamente el descriptor del controlador de usb-audio, lo que daba lugar a una lectura de memoria
    fuera de límites. Un atacante que pueda añadir dispositivos USB podría, posiblemente, usar
    esto para provocar denegación de servicio (caída).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

    <p>Hui Peng y Mathias Payer informaron de una recursión infinita en el
    código para analizar sintácticamente el descriptor del controlador de usb-audio, lo que daba lugar a desbordamiento
    de pila. Un atacante que pueda añadir dispositivos USB podría usar esto para
    provocar denegación de servicio (corrupción de memoria o caída) o, posiblemente,
    para elevación de privilegios. En la arquitectura amd64, y en la
    arquitectura arm64 en el caso de buster, esto se mitiga mediante una página de protección
    en la pila del núcleo, lo que hace que solo sea posible provocar una caída.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

    <p>Brad Spengler informó de que un error de adaptación reintrodujo una
    vulnerabilidad spectre-v1 en la función ptrace_get_debugreg() del
    subsistema ptrace.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 4.9.189-3+deb9u1.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.67-2+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
