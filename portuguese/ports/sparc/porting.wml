#use wml::debian::template title="Debian SPARC -- Documentação sobre o porte" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="f9d5abd797e762089776545824869e3e44bd2c42"

<h1>Documentação sobre o porte Debian SPARC</h1>

<h2>Portando pacotes Debian para SPARC</h2>
 <p>
Se você quiser ser um(a) portador(a) oficial, você deve ser um(a)
desenvolvedor(a) Debian registrado(a). Isto é, sua chave pública deve aparecer
no chaveiro (keyring) oficial.
 <p>
O esforço para portar o Debian para SPARC está agora organizado em torno do
excelente sistema <code>wanna-build</code>, usado pela primeira vez
para o porte <a href="../m68k/">m68k</a>. Com <code>wanna-build</code>,
portar resume-se a encontrar pacotes onde a compilação automática
falhou, e então revisar e determinar o que houve de errado.
 <p>
Logs de falhas da construção podem ser encontrados em
<a href="https://buildd.debian.org/status/architecture.php?a=sparc64">páginas web do buildd para SPARC 64</a>.
Também, você pode mandar um e-mail para <code>wanna-build</code> e perguntar
pelos logs de falha da construção (veja o arquivo <code>README.mail</code>
da distribuição <code>wanna-build</code>).
 <p>
Portadores(as) sérios(as) devem aprender como interagir com o
<code>wanna-build</code> via e-mail. Você precisará pedir a <a
href="mailto:bcollins@debian.org">Ben Collins
&lt;bcollins@debian.org&gt;</a> para adicionar sua chave pública
na lista de chaves conhecidas.
 <p>
Todos(as) desenvolvedores(as) Debian podem usar as
<a href="https://db.debian.org/machines.cgi">máquinas porterbox</a> do Debian
para testar seus pacotes na arquitetura SPARC.


<h2>Eu não sou um(a) desenvolvedor(a) oficial; ainda posso ajudar?</h2>
  <p>
Certamente. De fato, a maioria do trabalho real no porte do Debian não
requer status oficial, somente conhecimento. Existem inúmeras coisas
que você pode fazer:
<ul>
      <li>
Localizar bugs e reportá-los para o <a href="$(HOME)/Bugs/">
Sistema de Controle de Bugs (Bug Tracking System) Debian</a>.
      <li>
Descobrir correções (patches) para bugs conhecidos. Tenha certeza de mandar a
correção para o Sistema de Controle de Bugs!
      <li>
Ajuda com a documentação. A maioria das áreas de documentação são
gerenciadas em CVS, e a maioria dos(as) autores(as) das documentações
podem distribuir acesso CVS a não portadores(as) que estão interessados(as)
em ajudar.
    </ul>
  <p>
Então, vá em frente e envie um e-mail (em inglês) para <a
href="mailto:debian-sparc@lists.debian.org">&lt;debian-sparc@lists.debian.org&gt;</a>
com a descrição de como você gostaria de ajudar; garantimos que lá alguém
pode te ajudar a começar.


# <!-- Keep this comment at the end of the file
# Local variables:
# mode: sgml
# sgml-indent-data:nil
# sgml-doctype:"../../releases/.doctype"
# End:
# -->
