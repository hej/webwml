#use wml::debian::ddp title="Manuais de usuários(as) Debian"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/user-manuals.defs"
#use wml::debian::translation-check translation="35678d9c62667396da87b15a2475b03c7169ba26"

<document "FAQ do Debian GNU/Linux" "faq">

<div class="centerblock">
<p>
  Perguntas feitas frequentemente por usuários(as).

<doctable>
  <authors "Susan G. Kleinmann, Sven Rudolph, Santiago Vila, Josip Rodin, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debian-faq"><br>
  <inddpvcs-debian-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Guia de Instalação Debian" "install">

<div class="centerblock">
<p>
  Instruções de instalação para a distribuição Debian GNU/Linux.
  O manual descreve o processo de instalação usando o Debian Installer,
  o sistema de instalação para o Debian que foi lançado inicialmente
  com o <a href="$(HOME)/releases/sarge/">Sarge</a> (Debian GNU/Linux 3.1).<br>
  Informações adicionais sobre a instalação podem ser encontradas no
  <a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do Instalador Debian</a>
  e nas
  <a href="https://wiki.debian.org/DebianInstaller">páginas wiki do Instalador Debian</a>.

<doctable>
  <authors "Equipe do Debian Installer">

  <maintainer "Equipe do Debian Installer">
  <status>
    O manual ainda não está perfeito. Um trabalho contínuo é realizado tanto
    na versão atual quanto para futuras versões do Debian. Ajuda é bem-vinda,
    especialmente para instalação e traduções não-x86. Contate <a
    href="mailto:debian-boot@lists.debian.org?subject=Install%20Manual">\
    debian-boot@lists.debian.org</a> para mais informações.
  </status>
  <availability>
  <insrcpackage "installation-guide">
  <br><br>
  <a href="$(HOME)/releases/stable/installmanual">Versão publicada para a
  distribuição estável</a>
  <br>
  Disponível nos <a href="$(HOME)/CD/">CDs e DVDs oficiais completos</a>
  no diretório <tt>/doc/manual/</tt>.
  <br><br>
  <a href="$(HOME)/releases/testing/installmanual">Versão sendo preparada
  para a próxima versão estável (atualmente no testing)</a>
  <br><br>
  <a href="https://d-i.debian.org/manual/">Versão em desenvolvimento</a>
 <br>
  <inddpvcs-installation-guide>
  </availability>
</doctable>

<p>
Versões do guia de instalação para distribuições anteriores (e possivelmente
para o próximo lançamento) do Debian estão listadas a partir da
<a href="$(HOME)/releases/">página dos lançamentos</a> para estas versões.

</div>

<hr />

<document "Notas de Lançamento Debian" "relnotes">

<div class="centerblock">
<p>
  Este documento contém informações sobre o que há de novo na distribuição
  atual do Debian GNU/Linux e informações completas sobre como atualizar
  para usuários(as) de versões antigas do Debian.

<doctable>
  <authors "Adam Di Carlo, Bob Hilliard, Josip Rodin, Anne Bezemer, Rob Bradford, Frans Pop, Andreas Barth, Javier Fernández-Sanguino Peña, Steve Langasek">
  <status>
  Ativamente trabalhado para os lançamentos Debian.
  Contate
  <a href="mailto:debian-doc@lists.debian.org?subject=Release%20Notes">debian-doc@lists.debian.org</a>
  para mais informações.  Problemas e patches devem ser relatados como
  <a href="https://bugs.debian.org/release-notes">bugs contra o pseudopacote
  release-notes</a>.
  </status>
  <availability>
  <a href="$(HOME)/releases/stable/releasenotes">Versão lançada</a>
  <br>
  Disponível nos <a href="$(HOME)/CD/">CDs e DVDs oficiais completos</a>
  no diretório <tt>/doc/release-notes/</tt>.
#  <br>
#  Também disponível em <a href="$(HOME)/mirror/list">ftp.debian.org e todos os espelhos</a>
#  no diretório <tt>/debian/doc/release-notes/</tt>.
#  <br>
#  <a href="$(HOME)/releases/testing/releasenotes">Versão sendo preparada
#  para a próxima distribuição estável (atualmente em teste)</a>

  <inddpvcs-release-notes>
  </availability>
</doctable>
</div>

<hr />

<document "Cartão de Referência do Debian GNU/Linux" "refcard">

<div class="centerblock">
<p>
  Este cartão ajuda novos(as) usuários(as) do Debian GNU/Linux com
  os comandos mais importantes em uma única página para ser usado como referência
  quando estiverem trabalhando com Debian GNU/Linux.
  É necessário ter conhecimentos básicos (ou avançados) de computadores, arquivos,
  diretórios e linha de comando.

<doctable>
  <authors "W. Martin Borgert">
  <maintainer "W. Martin Borgert">
  <status>
  Publicado; em desenvolvimento ativo.
  </status>
  <availability>
  <inpackage "debian-refcard"><br>
  <inddpvcs-refcard>
  </availability>
</doctable>
</div>

<hr />

<document "O Manual do(a) Administrador(a) Debian" "debian-handbook">

<div class="centerblock">
<p>
    O Manual do(a) Administrador(a) Debian ensina o que é
    essencial para que qualquer pessoa se torne um(a) administrador(a) Debian
    GNU/Linux eficiente e independente.

<doctable>
  <authors "Raphaël Hertzog, Roland Mas">
  <status>
  Publicado; em desenvolvimento ativo.
  </status>
  <availability>
  <inpackage "debian-handbook"><br>
  <inddpvcs-debian-handbook>
  </availability>
</doctable>
</div>

<hr />

<document "Referência Debian" "quick-reference">

<div class="centerblock">
<p>
   Esta referência ao Debian GNU/Linux cobre muitos aspectos da administração
   do sistema através de exemplos em comandos do shell. Tutoriais básicos,
   dicas e outras informações são fornecidas para tópicos que incluem a
   instalação do sistema, o gerenciamento de pacotes Debian, o kernel Linux
   no Debian, configuração do sistema, construção de um gateway, editores de
   texto, CVS, programação e GnuPG.

   <p>Conhecido anteriormente como "Referência Rápida".

<doctable>
  <authors "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Publicado; em desenvolvimento ativo.
  </status>
  <availability>
  <inpackage "debian-reference"><br>
  <inddpvcs-debian-reference>
  </availability>
</doctable>
</div>

<hr />

<document "Manual de Segurança Debian" "securing">

<div class="centerblock">
<p>
   Este manual descreve a segurança do sistema operacional
   Debian GNU/Linux e a segurança interna do projeto Debian. Ele começa
   com o processo de assegurar e fortalecer a instalação Debian
   GNU/Linux padrão (tanto manual quanto automaticamente), cobre
   algumas das tarefas envolvidas em configurar um(a) usuário(a) seguro(a) e
   ambiente de rede, dá informações sobre as ferramentas de segurança
   disponíveis, passos a serem tomados antes e depois de um
   comprometimento, e também descreve como a segurança é implementada no
   Debian pela equipe de segurança. O documento inclui um guia de
   fortalecimento passo a passo e nos apêndices há informações
   detalhadas sobre como configurar um sistema de detecção de
   intrusões e uma bridge firewall com o Debian GNU/Linux.

<doctable>
  <authors "Alexander Reelsen, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  Publicado; em desenvolvimento com poucas mudanças. Parte do seu conteúdo pode
  não estar totalmente atualizado.
  </status>
  <availability>
  <inpackage "harden-doc"><br>
  <inddpvcs-securing-debian-manual>
  </availability>
</doctable>
</div>

<hr />

<document "Manual do(a) usuário(a) do aptitude" "aptitude">

<div class="centerblock">
<p>
    Um guia introdutório sobre o administrador de pacotes aptitude, incluindo
    uma referência completa de comandos.


<doctable>
  <authors "Daniel Burrows">
  <status>
  Publicado; em desenvolvimento ativo.
  </status>
  <availability>
  <inpackage "aptitude-doc"><br>
  <inddpvcs-aptitude>
  </availability>
</doctable>
</div>

<hr />

<document "Guia do(a) usuário(a) do APT" "apt-guide">

<div class="centerblock">
<p>
    Este documento oferece uma visão geral de como usar o administrador de
    pacotes APT.


<doctable>
  <authors "Jason Gunthorpe">
  <status>
  Publicado; desenvolvimento parado.
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-guide>
  </availability>
</doctable>
</div>

<hr />

<document "Usando o APT Offline" "apt-offline">

<div class="centerblock">
<p>
    Este documento descreve como usar o APT num ambiente fora de rede,
    especialmente numa abordagem 'sneaker-net' para atualizações de performance.

<doctable>
  <authors "Jason Gunthorpe">
  <status>
  Publicado; desenvolvimento parado.
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-offline>
  </availability>
</doctable>
</div>

<hr />

<document "FAQ do Debian e Java" "java-faq">

<div class="centerblock">
<p>
O propósito desse FAQ é ser um lugar de referência para todo tipo de
perguntas que um(a) desenvolvedor(a) ou um(a) usuário(a) pode ter sobre o Java
em relação ao Debian. Ele inclui problemas de licenças, pacotes de
desenvolvimento disponíveis e programas relacionados com a construção
de um ambiente Java feito de Software Livre.

<doctable>
  <authors "Javier Fernández-Sanguino Peña, Torsten Werner, Niels Thykier, Sylvestre Ledru">
  <status>
  Publicado; em desenvolvimento ativo, apesar de que algum conteúdo pode
  não estar atualizado.
  </status>
  <availability>
  <inpackage "java-common"><br>
  <inddpvcs-debian-java-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Guia do(a) Mantenedor(a) do pacote Rádio Amador Portátil Debian" "hamradio-maintguide">

<div class="centerblock">
<p>
o Guia do(a) Mantenedor(a) do pacote Rádio Amador Portátil Debian é um documento
que descreve políticas de equipe e boas práticas para o time de mantenedores(as)
do pacote Rádio Amador Portátil Debian.

<doctable>
  <authors "Iain R. Learmonth">
  <status>
  Publicado; em desenvolvimento ativo.
  </status>
  <availability>
  <inpackage "hamradio-maintguide"><br>
  <inddpvcs-hamradio-maintguide>
  </availability>
</doctable>
</div>
