#use wml::debian::template title="Debian &ldquo;lenny&rdquo; -- Informações de instalação" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/lenny/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Instalando o Debian GNU/Linux <current_release_lenny></h1>

<div class="important">
<p><strong>O Debian GNU/Linux 5.0 foi substituído pelo
<a href="../../squeeze/">Debian GNU/Linux 6.0 (<q>squeeze</q>)</a>. Algumas
destas imagens de instalação podem não estar disponíveis, ou podem não
funcionar mais, é recomendado que você instale o squeeze em vez disso.
</strong></p>
</div>

<p>
<strong>Para instalar o Debian GNU/Linux</strong> <current_release_lenny>
(<em>etch</em>), baixe qualquer uma das seguintes imagens:
</p>

<div class="line">
<div class="item col50">
	<p><strong>imagem de CD netinst (geralmente 135-175 MB)</strong></p>
		<netinst-images />
</div>

<div class="item col50 lastcol">
	<p><strong>imagem de CD businesscard (geralmente 20-50 MB)</strong></p>
		<businesscard-images />
</div>

</div>

<div class="line">
<div class="item col50">
	<p><strong>conjuntos completos de CDs</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>conjuntos completos de DVDs</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>outras imagens (netboot, pendrive usb, etc)</strong></p>
<other-images />
</div>
</div>

<p>
Se algum hardware do seu sistema <strong>requerer firmware não livre para ser
carregado</strong> com o controlador de dispositivos, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/lenny/current/">\
arquivos tarball de pacotes de firmwares comuns</a>. Instruções de como usar
esses arquivos tarball e informações gerais sobre carregamento de firmware
durante a instalação podem ser encontradas no guia de instalação
(veja documentação abaixo).
</p>

<p>
<strong>Notas</strong>
</p>
<ul>
#   <if-lennynhalf-released released="yes"><li>
#      Informações sobre a <strong>instalação do Debian GNU/Linux
#      <q>lenny-and-a-half</q></strong> (usando um kernel 2.6.24 atualizado)
#      estão disponíveis a partir de uma <a href="lennynhalf">página separada</a>.
#   </li></if-lennynhalf-released>
    <li>
	Para baixar imagens completas de CD e DVD, recomenda-se usar
	o BitTorrent ou o jigdo.
    </li><li>
	Para arquiteturas pouco comuns, somente um número limitado de imagens
	dos conjuntos de CD e DVD está disponível como um arquivo ISO ou via
	BitTorrent. Os conjuntos completos estão disponíveis somente via jigdo.
    <li>
        As imagens de <em>CD</em> multiarquitetura suportam i386/amd64/powerpc
	e alpha/hppa/ia64 respectivamente; a instalação é similar a instalar
	a partir de uma imagem netinst para uma única arquitetura.
    </li><li>
        As imagens de <em>DVD</em> multiarquitetura suportam i386/amd64/powerpc;
	a instalação é similar a instalar a partir de uma imagem de CD completa
	para uma única arquitetura; o DVD também contém o código-fonte para
	todos os pacotes incluídos.
    </li><li>
        Para as imagens de instalação, arquivos de verificação
        (<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> e outros) estão disponíveis a
        partir do mesmo diretório das imagens.
    </li>
</ul>


<h1>Documentação</h1>

<p>
<strong>Se você lê somente um documento</strong> antes da instalação, leia
nosso <a href="../i386/apa">Howto de instalação</a>, um rápido passo a passo
do processo de instalação. Outras documentações úteis incluem:
</p>

<ul>
<li><a href="../installmanual">Guia de instalação do lenny</a><br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do instalador do
Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do Debian-CD</a><br />
perguntas comuns e respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
documentação mantida pela comunidade</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Esta é uma lista de problemas conhecidos no instalador que acompanha o
Debian GNU/Linux <current_release_lenny>. Se você teve algum problema
instalando o Debian e não vê seu problema listado aqui, por favor, envie-nos um
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">relatório de
instalação</a> descrevendo o problema ou
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">verifique a
wiki</a> para outros problemas conhecidos.
</p>

<h3 id="errata-r0">Errata para a versão 5.0</h3>

<dl class="gloss">
	<dt>A montagem automática de arrays RAID no modo de recuperação pode
	corromper os dados</dt>
	<dd>
        O modo de recuperação deve ser usado com muito cuidado quando software
        de arrays RAID estiverem em uso no sistema para recuperação. Os scripts
        do modo de recuperação montam automaticamente os arrays, o que pode
        levar à corrupção de dados na presença de superblocos RAID inválidos ou
        obsoletos.
	</dd>

	<dt>Exibição corrompida de mensagens nas instalações do Dzongkha</dt>
	<dd>
        Quando a senha escolhida para root e suas confirmações não coincidem,
        a exibição da tela a seguir é ilegível durante as instalações no
        idioma Dzongkha (exibição quebrada da fonte itálica).
	</dd>

	<dt>Os dispositivos de disco podem mudar na reinicialização</dt>
	<dd>
	Em sistemas com vários controladores de disco, o kernel/udev pode
	atribuir um nó de dispositivo diferente na reinicialização do sistema
	ao ser usado durante a instalação devido à diferença na ordem de
	carregamento dos drivers.<br />
	Isso pode levar à falha na inicialização do sistema. Na maioria dos
	casos isso pode ser corrigido alterando a configuração do gerenciador
	de inicialização e do /etc/fstab, possivelmente usando o modo de
	recuperação do instalador.<br />
	Observe no entanto que esse problema pode ocorrer novamente em
	inicializações subsequentes.
	</dd>

	<dt>Problemas de reinicialização ao instalar a partir de um pendrive
	USB</dt>
	<dd>
	O problema anterior também pode acontecer ao instalar a partir de um
	pendrive USB.
	Manter temporariamente o pendrive USB no lugar permitirá inicializar
	o sistema instalado e corrigir o arquivo de configuração do carregador
	de inicialização. Consulte
	<a href="https://bugs.debian.org/506263">#506263</a> para obter detalhes
	sobre essa solução alternativa.
	</dd>

	<dt>Roteadores com bugs podem causar problemas de rede</dt>
	<dd>
	Se você tiver problemas de rede durante a instalação, isso pode ser
	causado por um roteador em algum lugar entre você e o espelho Debian
	que não lida corretamente com dimensionamento da janela.
	Veja <a href="https://bugs.debian.org/401435">#401435</a> e esse
	<a href="http://kerneltrap.org/node/6723">artigo kerneltrap</a> para
	detalhes.<br />
	Você pode contornar esse problema desabilitando o dimensionamento da
	janela TCP. Abra um shell e digite o seguinte comando:<br />
	<tt>echo 0 &gt; /proc/sys/net/ipv4/tcp_window_scaling</tt><br />
        Para o sistema instalado você provavelmente não deve desativar
        completamente o dimensionamento da janela TCP. O comando a seguir
        definirá intervalos de leitura e gravação que devem funcionar com
        praticamente qualquer roteador:<br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_rmem</tt><br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_wmem</tt>
	</dd>

	<dt>Não é utilizável na instalação do Squeeze ou Sid</dt>
	<dd>
	Devido a alterações no pacote <tt>passwd</tt> no teste (testing) e
	instável (unstable), a configuração de uma conta de usuário(a) falhará.
	Para detalhes veja <a href="https://bugs.debian.org/529475">#529475</a>.
	</dd>

<!-- deixando isso para possível uso futuro...
	<dt>i386: é necessário mais de 32 mb de memória para instalar</dt>
	<dd>
	A quantidade mínima de memória necessária para instalar com êxito no
	i386 é 48 mb, em vez dos 32 mb anteriores. Esperamos reduzir os
	requisitos de volta para 32 mb posteriormente. Os requisitos de memória
	também podem ter sido alterados para outras arquiteturas.
	</dd>
-->

	<dt>i386: vários problemas</dt>
	<dd>
	O porte i386 tem alguns problemas conhecidos nesta versão:
	<ul>
	<li>Devido a um aumento no tamanho do kernel do Linux, não podemos
	fornecer imagens de instalação para instalações a partir do
	disquete.</li>
	<li>Tivemos pelo menos um relatório sobre o instalador travando na
	  etapa de detecção de hardware de rede em alguns laptops Dell Inspiron.
	  Veja <a href="https://bugs.debian.org/509238">bug #509238</a>
	  para obter detalhes. Uma solução alternativa parece ser inicializar
	  o instalador com o parâmetro <q>vga = 771</q>.</li>
	  </ul>
	</dd>

	<dt>PowerPC: vários problemas</dt>
	<dd>
	O porte PowerPC tem vários problemas nesta versão:
	<ul>
		<li>a instalação a partir do disquete no PowerMac OldWorld
		    está quebrada porque nenhum nó do dispositivo foi criado
		    para o módulo swim3 e assim como o miboot não está
		    incluído</li>
		<li>o módulo snd-powermac não é mais carregado por padrão, pois
		    trava alguns sistemas; você precisará adicioná-lo ao
		    <tt>/etc/modules</tt> manualmente</li>
	</ul>
	</dd>

	<dt>s390: recursos não suportados</dt>
	<dd>
	<ul>
		<li>o suporte para a disciplina DASD DIAG não está disponível
		    no momento</li>
                <li>o suporte para interfaces de rede LCS não está mais
                disponível</li>
	</ul>
	</dd>
</dl>
