#use wml::debian::translation-check translation="a4e1569c494a4c453a82c868fa62cec78e894157" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag pagetitle>데비안 설치관리자 Buster RC 2 릴리스</define-tag>
<define-tag release_date>2019-06-26</define-tag>
#use wml::debian::news

<p>
데비안 설치관리자 <a
href="https://wiki.debian.org/DebianInstaller/Team">팀</a>은 데비안 10 설치관리자 <q>Buster</q>의 2번째 후보를 알리게 되어 기쁩니다.
</p>


<h2>이 릴리스에서 항상된 것</h2>

<ul>
  <li>choose-mirror:
    <ul>
      <li>Update Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>cryptsetup:
    <ul>
      <li>New section “Unlocking LUKS devices from GRUB” pointing
        to: <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html</a></li>
    </ul>
  </li>
  <li>debian-archive-keyring:
    <ul>
      <li>Add Buster keys (<a href="https://bugs.debian.org/917535">#917535</a>, <a href="https://bugs.debian.org/917536">#917536</a>).</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Create images to fit on 16G USB sticks too (for amd64 and
        i386).</li>
      <li>Tweak package selection to make the multi-arch firmware
        netinst fit on CD media again (needs a 700MB CD-R): Don't
        include 686 PAE kernels on these CDs.</li>
      <li>Tweak ordering of snapshot URLs in jigdo images to remove
        load on snapshot.debian.org.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Add haveged-udeb [linux] to avoid entropy starvation issues
        (<a href="https://bugs.debian.org/923675">#923675</a>). Those could affect HTTPS connections, SSH keypair
        generation, etc.</li>
      <li>Bump Linux kernel ABI from 4.19.0-4 to 4.19.0-5.</li>
      <li>Add œ/Œ glyphs for the French translation.</li>
      <li>Update size limits.</li>
      <li>Relabel “Dark theme” into “Accessible high contrast”
        (<a href="https://bugs.debian.org/930569">#930569</a>).</li>
      <li>Compress armhf u-boot images with “gzip -n” to avoid
        embedding timestamps which cause reproducibility issues.</li>
    </ul>
  </li>
  <li>espeakup:
    <ul>
      <li>Wait longer for sound cards.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Make grub-efi-*-bin recommend efibootmgr, for debugging
        purposes.</li>
      <li>Make grub-efi work on armhf too (upstream fixes for
        alignment bugs).</li>
    </ul>
  </li>
  <li>installation-guide:
    <ul>
      <li>Add the partman-auto-lvm/guided_size setting to the example
        preseed config file (<a href="https://bugs.debian.org/930846">#930846</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer:
    <ul>
      <li>Enlarge maximum line length in Packages and Sources files
        (<a href="https://bugs.debian.org/554444">#554444</a>).</li>
    </ul>
  </li>
  <li>lowmem:
    <ul>
      <li>Update size limits.</li>
    </ul>
  </li>
  <li>network-console:
    <ul>
      <li>Fix gen-crypt segfault, which prevented remote installations
        due to a missing password for the “installer” user (<a href="https://bugs.debian.org/926947">#926947</a>,
        <a href="https://bugs.debian.org/928299">#928299</a>).</li>
    </ul>
  </li>
  <li>openssl:
    <ul>
      <li>Ship an openssl.cnf in libssl1.1-udeb, fixing wget's TLS
        issues in the installer (<a href="https://bugs.debian.org/926315">#926315</a>).</li>
    </ul>
  </li>
  <li>partman-auto:
    <ul>
      <li>Tweak Arabic translation to avoid a hang at the hard disk
        step (<a href="https://bugs.debian.org/929877">#929877</a>).</li>
    </ul>
  </li>
  <li>preseed:
    <ul>
      <li>Update auto-install/defaultroot, replacing stretch with
        buster (<a href="https://bugs.debian.org/928031">#928031</a>).</li>
    </ul>
  </li>
  <li>rootskel:
    <ul>
      <li>Start haveged when appropriate, to avoid entropy starvation
        (<a href="https://bugs.debian.org/923675">#923675</a>). This means when the haveged binary is available,
        and when there's no hardware RNG available.</li>
      <li>Update size limits for the graphical installer.</li>
    </ul>
  </li>
</ul>


<h2>UEFI 보안 부트 업데이트</h2>

<p>Debian's Secure Boot setup is still being polished, the main
  updates are summarized below.</p>

<ul>
  <li>debian-installer:
    <ul>
      <li>Add shim-signed and grub-efi-ARCH-signed to
        build-dependencies for amd64/i386/arm64.</li>
      <li>Use the signed shim and grub packages for all 3 arches for
        EFI images.</li>
      <li>Fix the netboot setup for signed grub images to match the
        previous setup and the existing documentation (<a href="https://bugs.debian.org/928750">#928750</a>).</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Generate a specific signed netboot image for d-i to use
        (<a href="https://bugs.debian.org/928750">#928750</a>).</li>
      <li>Add cpuid, play, ntfs modules to signed UEFI images
        (<a href="https://bugs.debian.org/928628">#928628</a>, <a href="https://bugs.debian.org/930290">#930290</a>, <a href="https://bugs.debian.org/923855">#923855</a>).</li>
      <li>Deal with --force-extra-removable with signed shim too
      (<a href="https://bugs.debian.org/930531">#930531</a>).</li>
    </ul>
  </li>
</ul>


<h2>하드웨어 지원 바뀜</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>[arm64] Add support for netboot SD-card-images.</li>
      <li>[arm64] Add u-boot images for a64-olinuxino,
        orangepi_zero_plus2 and teres_i.</li>
      <li>Add support for NanoPi NEO2.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add support for NanoPi NEO2 (<a href="https://bugs.debian.org/928861">#928861</a>).</li>
      <li>Add support for Marvell 8040 MACCHIATOBin Double-shot and
        Single-shot (<a href="https://bugs.debian.org/928951">#928951</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>udeb: Add all HWRNG drivers to kernel-image (<a href="https://bugs.debian.org/923675">#923675</a>).</li>
      <li>udeb: input-modules: Include all keyboard driver
        modules.</li>
      <li>[arm64] udeb: kernel-image: Include cros_ec_spi and SPI
        drivers.</li>
      <li>[arm64] udeb: kernel-image: Include phy-rockchip-pcie.</li>
      <li>[arm64] udeb: usb-modules: Include phy-rockchip-typec and
        extcon-usbc-cros-ec.</li>
      <li>[arm64] udeb: mmc-modules: Include phy-rockchip-emmc.</li>
      <li>[arm64] udeb: fb-modules: Include rockchipdrm, panel-simple,
        pwm_bl, and pwm-cros-ec.</li>
      <li>udeb: Drop unused ntfs-modules packages.</li>
    </ul>
  </li>
</ul>


<h2>지역화 상태</h2>

<ul>
  <li>76개 언어를 이 릴리스에서 지원.</li>
  <li>그 중 39개 완전 번역.</li>
</ul>


<h2>이 릴리스에서 알려진 이슈</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>이 릴리스에 대한 피드백</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>감사</h2>

<p>데비안 설치관리자 팀은 이 릴리스에 기여한 모든 분에게 감사드립니다.</p>
